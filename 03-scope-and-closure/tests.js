describe("generatorFactory", function() {

  it("je funkce která vrací jinou funkci (TODO 1.2)", function() {
    let returnValueType = typeof baseGeneratorFactory();

    expect(returnValueType).toBe("function");
  });

  it("bez parametrů vrací funkci, která vrátí 0 při prvním volání (TODO 1.2)", function() {
    let generator = baseGeneratorFactory();

    expect(generator()).toBe(0);
  });

});

describe("generator", function() {
  it("vrací při prvním volání initialNumber factory, ze které vznikl (TODO 1.2)", function() {
    let generatorFromTen = baseGeneratorFactory(10);

    expect(generatorFromTen()).toBe(10);
  });

  it("vrací zvýšené číslo při každém dalším volání (TODO 1.3)", function() {
    let generatorFromTen = baseGeneratorFactory(100);

    expect(generatorFromTen()).toBe(100);
    expect(generatorFromTen()).toBe(101);
    expect(generatorFromTen()).toBe(102);
    expect(generatorFromTen()).toBe(103);
  });

});

describe("stepGeneratorFactory", function() {

  it("vrací objekt s vlasnostmi setStep a increase (TODO 2.1)", function() {
    let generator = stepGeneratorFactory(0);

    expect(generator.setStep).toBeDefined();
    expect(generator.increase).toBeDefined();
  });

  it("setStep je funkce s jedním parametrem  (TODO 2.2)", function() {
    let generator = stepGeneratorFactory(0);

    expect(typeof generator.setStep).toBe("function");
    expect(generator.setStep.length).toBe(1);
  });

  it("increase je funkce, vrací hodnotu zvýšenou o step  (TODO 2.3)", function() {
    let generator = stepGeneratorFactory(0);

    expect(typeof generator.increase).toBe("function");

    generator.setStep(10);

    expect(generator.increase()).toBe(10);
    expect(generator.increase()).toBe(20);
  });

});
