export class Operation {
  constructor() {
    this.name = this.constructor.name.toLowerCase();
  }

  calculate(a, b) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };

}

export class Add extends Operation {
  calculateInternal_(a, b) {
    return a + b;
  };
}

export class Sub extends Operation {
  calculateInternal_(a, b) {
    return a - b;
  };
}

export class Div extends Operation {
  calculateInternal_(a, b) {
    return a / b;
  };
}

export class Mul extends Operation {
  calculateInternal_(a, b) {
    return a * b;
  };
}
