export class ButtonControls {
  constructor(buttonsElement, calculator, display) {

    this.calculator = calculator;
    this.display = display;

    buttonsElement.addEventListener("click", (event) => this.processButton(event));
  }

  processButton(event) {

    const button = event.target;

    if (button.type !== "button") {
      return;
    }

    if (button.classList.contains("operation")) {
      this.processOperation_(button.name);
    } else {
      const value = button.getAttribute("value");
      this.processNumber_(value);
    }
  }

  processOperation_(type) {

    switch (type) {
      case "clear":
        this.calculator.clear();
        this.display.clear();
        break;

      case "equals":
        const result = this.calculator.equals();
        this.display.setValue(result);
        break;

      default:
        this.display.setValue(this.display.getValue());
        this.calculator.setOperand(this.display.getValue());

        this.calculator.setOperation(type);
    }
  }

  processNumber_(value) {
    this.display.addToValue(value);
    this.calculator.setOperand(this.display.getValue());
  }
}
