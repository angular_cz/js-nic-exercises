export abstract class Operation {
  readonly name: string;

  constructor() {
    this.name = this.constructor.name.toLowerCase();
  }

  calculate(a: string, b: string) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };

  protected abstract calculateInternal_(a: number, b: number): number;

}

export class Add extends Operation {
  calculateInternal_(a: number, b: number) {
    return a + b;
  };
}

export class Sub extends Operation {
  calculateInternal_(a: number, b: number) {
    return a - b;
  };
}

export class Div extends Operation {
  calculateInternal_(a: number, b: number) {
    return a / b;
  };
}

export class Mul extends Operation {
  calculateInternal_(a: number, b: number) {
    return a * b;
  };
}
