export class Display {

  constructor(displayElement) {

    this.isResult_ = false;
    this.element_ = displayElement;  // TODO check
    this.value_ = null;

    this.clear();
  }

  setValue(value) {
    this.isResult_ = true;
    this.value_ = value;
    this.render_();
  }

  addToValue(value) {
    if (this.isResult_) {
      this.setValue("");
      this.isResult_ = false;
    }

    this.value_ += value;
    this.render_();
  }

  getValue() {
    return this.value_;
  }

  clear() {
    this.setValue("0");
  }

  render_() {
    this.element_.setAttribute("value", this.value_);
  }
}
