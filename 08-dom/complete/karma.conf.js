var baseConfig = require('../../base-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);

  config.set({
    basePath: '../../'
  });

  config.files.push('08-dom/transpiled/complete/calculations/operations.js');
  config.files.push('08-dom/transpiled/complete/calculations/calculator.js');
  config.files.push('08-dom/transpiled/complete/ui-components/display.js');
  config.files.push('08-dom/transpiled/complete/ui-components/buttonControls.js');

  config.files.push('08-dom/transpiled/complete/calculations/calculator.spec.js');
  config.files.push('08-dom/transpiled/complete/ui-components/display.spec.js');
  config.files.push('08-dom/transpiled/complete/ui-components/buttonControls.spec.js');
};

