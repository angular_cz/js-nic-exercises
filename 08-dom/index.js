import { Calculator } from './calculations/calculator'
import { Add, Sub, Mul, Div } from './calculations/operations'
import { ButtonControls} from './ui-components/buttonControls'
import { Display } from './ui-components/display'

export function initCalculator(displayElement, buttonsElement) {

  const calculator = new Calculator();
  calculator.addOperation(new Add());
  calculator.addOperation(new Sub());
  calculator.addOperation(new Div());
  calculator.addOperation(new Mul());

  // TODO 1.5 - Použijte Display ve view
  let display;

  // TODO 4 - Použijte ButtonControls ve view

}
