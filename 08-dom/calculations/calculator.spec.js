import { Calculator } from './calculator'
import { Add } from './operations'

describe('Calculator', function() {

  beforeEach(function() {
    this.calculator = new Calculator();
    this.calculator.addOperation(new Add());
  });

  it('má úvodní hodnotu 0', function() {
    expect(this.calculator.equals()).toBe(0);
  });

  it('nabývá hodnoty operandu při zadání', function() {
    "use strict";
    this.calculator.setOperand(42);
    expect(this.calculator.equals()).toBe(42);
  });

  it('nabývá hodnoty posledního operandu při zadání', function() {
    "use strict";
    this.calculator.setOperand(4);
    this.calculator.setOperand(42);
    expect(this.calculator.equals()).toBe(42);
  });

  describe('po vybrání akce', function() {
    it('počítá s předchozím operandem', function() {
      this.calculator.setOperand(1);
      this.calculator.setOperation('add');
      expect(this.calculator.equals()).toBe(2);
      expect(this.calculator.equals()).toBe(3);
      expect(this.calculator.equals()).toBe(4);
    });

    it('a zadáním operandu počítá s ním', function() {
      this.calculator.setOperand(42);
      this.calculator.setOperation('add');
      this.calculator.setOperand(1);
      expect(this.calculator.equals()).toBe(43);
      expect(this.calculator.equals()).toBe(44);
      expect(this.calculator.equals()).toBe(45);
    });
  });

  describe('po vymazání', function() {
    it('má výsledek 0', function() {
      this.calculator.clear();

      expect(this.calculator.equals()).toBe(0);
    });

    it('má výsledek 0 i pokud byla předtím zadána akce', function() {
      this.calculator.setOperand(42);
      this.calculator.setOperation('add');

      this.calculator.clear();

      expect(this.calculator.equals()).toBe(0);
    });
  });

});
