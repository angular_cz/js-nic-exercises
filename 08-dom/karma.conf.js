var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });
  config.files.push('08-dom/transpiled/calculations/operations.js');
  config.files.push('08-dom/transpiled/calculations/calculator.js');
  config.files.push('08-dom/transpiled/ui-components/display.js');
  config.files.push('08-dom/transpiled/ui-components/buttonControls.js');

  config.files.push('08-dom/transpiled/calculations/calculator.spec.js');
  config.files.push('08-dom/transpiled/ui-components/display.spec.js');
  config.files.push('08-dom/transpiled/ui-components/buttonControls.spec.js');
};

