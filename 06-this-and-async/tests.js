import {Countdown} from './index'

describe('Odpočet', function() {

  it('nastavuje intervalID (TODO 1.2)', function() {
    const countdown = new Countdown(1);
    countdown.start(function() {
  });

    expect(countdown.intervalID).not.toBeNull();
  });

  describe("callback", function() {

    let asyncSpec;

    // TODO 1.3 - Zrychlete testy - instalujte modul clock
    beforeEach(function() {

    });

    afterEach(function() {

    });

    // asyncSpec slouží pro účely lepší detekce splnění zadání, v praxi není třeba test ukládat do proměnné

    asyncSpec = it('update volá po vteřině (TODO 1.1) (TODO 1.2)', function() {

      const countdown = new Countdown(1);

      const updateCallback = function(value) {
        // TODO 1.1 - Test pro odpočet - ověřte snížení hodnoty

      };

      countdown.start(updateCallback);

      // TODO 1.3 - Zrychlete test - Poskočte o 1001
    });

    it('přijímá done callback', function() {
      expect(asyncSpec.queueableFn.fn.length).toBe(1);
    });

    it('volá done callback', function() {
      expect(asyncSpec.queueableFn.fn.toString()).toMatch(/done\W?\(\)/igm);
    });

    it('final volá po skončení odpočtu (TODO 1.3)', function(done) {

      // TODO 1.3 - Odstraňte pending
      pending();

      const countdown = new Countdown(3);

      countdown.start(null, function(value) {
        expect(value).toBe(3);
        done();
      });

      // TODO 1.3 - Zrychlete testy - Poskočte o 4000
    });

    it('final nevolá, pokud byl odpočet zastaven (TODO 1.5)', function(done) {

      // TODO 1.4 - Odstraňte pending
      pending();
      const countdown = new Countdown(3);

      countdown.start(null, function() {
        // TODO 1.4 - finalCallback nemá být volán po zastavení odpočtu metodou stop
      });

      expect(countdown.isRunning()).toBe(true);

      countdown.stop();

      jasmine.clock().tick(4000);
      done();
    });

  });

});
