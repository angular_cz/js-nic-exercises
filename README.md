## Javascript cvičení - Angular.cz

## Co si nainstalovat
- git - http://git-scm.com/downloads
- aktuální verzi nodeJS - http://nodejs.org/download/

Pokud instalujete Git na Windows, zvolte možnost pro instalaci i do Windows command prompt - viz [obrázek](http://www.foryt.com/wp-content/uploads/2014/08/Git-Setup-Run-from-Windows-Command-Prompt.png) 

## NPM závislosti
Nodejs se nainstaloval spolu s balíčkovacím nástrojem npm.

## Ověření správnosti instalace

Stáhněte tento balíček kamkoli k sobě. 

```
git clone https://bitbucket.org/angular_cz/js-nic-exercises
```

Spusťte v jeho kořenovém adresáři následující příkazy, žádný z nich by neměl hlásit chybu (první může chvíli trvat).

```
 npm install

 npm start
```

Nyní, když otevřete prohlížeč na adrese http://localhost:8000/, uvidíte uvítací obrazovku Školení - Javascript - cvičení.
Pokud se stránka zobrazila, je nastylovaná, vlevo vidíte menu, pak je vše v pořádku. Jste na školení připraven.

Pokud došlo během instalace nebo spuštění k nějaké chybě, podívejte se do sekce **Možné problémy**, případně nás kontaktujte na [angular@angular.cz](mailto:angular@angular.cz)

### Volba editoru ###

Otevřete si složku cvičení ve vašem oblíbeném editoru, tak, abyste viděli všechny příklady jako strom.

Velmi Vám doporučujeme použít editor, který umí syntaxi javascriptu a automatické formátování. Pokud to Váš editor neumí, použijte:

 - Webstorm - [https://www.jetbrains.com/webstorm/download/] (https://www.jetbrains.com/webstorm/download/) - 30 dní trial

#### Poznámka k editorům ####
Některé editory (například Webstorm) ukládají soubory tak, že se jej nejprve zapíšou do dočasného úložiště a poté překopírují na cílové místo. Toto způsobuje problémy při sledování změn souborů, pro účely kurzu tuto vlastnost vypněte (Webstorm ji označuje jako "safe write")

## Použití při cvičeních: ##

Všechny potřebné nástroje a závislosti jste nainstalovali už v předchozích krocích pomocí příkazu "npm install".

### Při dalším spouštění už si tak vystačíte s příkazem: ###
```
npm start
```
který spustí lokální server na adrese http://localhost:8000/

### Spuštění testů v jednotlivých cvičeních ###
V některých cvičeních se testy spouštějí externím nástrojem (karma runner). Spustíte je pomocí následujících příkazů. Příkazy se spouští v kořenovém adresáři balíčku.

#### 07-async-promise ####
```
npm run tests-async
```

#### 09-dom ####
```
npm run tests-dom
```

#### 10-react ####
```
npm run tests-react
```

### Možné probémy ###

####unable to connect to github.com####
pokud vidíte tuto chybovou zprávu po spuštění *npm install*

* máte buď blokováno připojení ke githubu  - to můžete ověřit otevřením github.com v prohlížeči
* nebo máte blokován protokol git - spusťte příkaz, který "přesměruje" protokol git po https

```
git config url."https://github.com/".insteadOf git@github.com:
git config url."https://".insteadOf git://
```

Nyní už by měl příkaz *npm install* fungovat

Pokud problémy přetrvávají, a jste uživatelem systému Windows, může zde být následující problém:

* Nastavení výše se zapíšou do .gitconfig do domovské složky, na Windows s profilem na vzdáleném disku však může každý ze shelů hledat home jinde. Pokud je toto Váš případ, zkopírujte soubor do obou umístění, na sdílenou domovskou složku a c:\users\[name]\

Chcete-li používat konfiguraci i pro další projekty, můžete ji nastavit globálně (přidat atribut --global)

```
git config --global url."https://github.com/".insteadOf git@github.com:
git config --global url."https://".insteadOf git://
```
Nyní už by měl příkaz npm install fungovat.