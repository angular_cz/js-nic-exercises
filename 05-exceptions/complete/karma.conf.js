var baseConfig = require('../../base-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);

  config.set({
    basePath: '../../'
  });

  config.files.push('05-exceptions/transpiled/complete/calculator.js');
  config.files.push('05-exceptions/transpiled/complete/tests.js');

};

