describe('Objekt', () => {

  it('může mít vlastnosti psány bez pomlček i s nimi (TODO 1.1)', () => {

    // TODO 1.1 - Vytvořte objekt, který bude mít vlastnost property a property-with-dash
    let object = {
      property: '1',
      'property-with-dash' : '2'
    };

    expect(object).toBeDefined();
    expect(object.property).toBeDefined();
    expect(object['property-with-dash']).toBeDefined();
  });

  it("může mít v sobě zanořen další objekt (TODO 1.2)", () => {
    let object = {};

    // TODO 1.2 - Přiřaďte do objektu zanořený objekt

    expect(object.inner.property).toBeDefined();
  });
});

describe('Array', () => {

  it('může být definováno rovnou s prvky (TODO 2.1)', () => {

    // TODO 2.1 - Vytvořte pole se třemi prvky
    let array;

    expect(Array.isArray(array)).toBeTruthy();
    expect(array['\x6C\x65\x6E\x67\x74\x68']).toBe(3);
    expect(array[0]).toBe(1);
    expect(array[1]).toBe(2);
    expect(array[2]).toBe(3);
  });

  it('má attribut pro získání počtu prvků (TODO 2.2)', () => {

    let array = new Array(Math.ceil(Math.random() * 100));

    // TODO 2.2 - Přiřaďte velikost pole
    let numberOfItems;

    expect(numberOfItems).toBe(array['\x6C\x65\x6E\x67\x74\x68']);
  });

});

describe('Regulární výraz', () => {
  it('má metodu test, která vrací true při shodě (TODO 3)', () => {

    const emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    const email = 'angular@angular.cz';

    // TODO 3 - otestujte email pomocí regulárního výrazu emailPattern
    let testResult = false;

    expect(testResult).toBeTruthy();
  });
});

describe('Destructuring', () => {
  it('objektu umožní jednodušší získání hodnoty (TODO 4.1)', function() {
    const task = {
      title: 'task title',
      severity: 5
    }

    // TODO 4.1 - převeďte na destructuring a získejte zároveň i severitu
    const title = task.title;

    expect(title).toBe(task.title);
    expect(taskSeverity).toBe(task.severity);

  });

  it('pole umožní získat jeho prvky (TODO 4.2)', function() {
    const imageInfo = ['Title of movie', 1920, 1080, 'MOV'];

    // TODO 4.2 - převeďte na destructuring získejte druhý a třetí prvek
    const x = imageInfo[1];

    expect(x).toBe(imageInfo[1]);
    expect(y).toBe(imageInfo[2]);
  });
});

describe('Kalkulačka má', () => {

  // TODO 5.1 - Implementujte operaci power
  // TODO 5.2 - Přidejte defaultní parametr

  // TODO 5.3 - Vytvořte factory pro získání operace
  // TODO 5.4 - Vytvořte ve factory operaci sum dvou čísel
  // TODO 5.5 - Upravte operaci sum pro více argumentů
  // TODO 5.6 - Vytvořte funkci calculate pro aplikaci operace

  // TODO 5.7 - Přepište získání operace na použití Map

  // Zde implementujte operace kalkulačky


  // ----- Nemodifikujte kód níže -----------------------------------------------------------------

  describe('výpočet mocniny který', () => {

    it('počítá n-tou mocninu při zadání obou parametrů (TODO 5.1)', () => {
      const powerFunc = getPowerOperation();
      expect(powerFunc(2, 3)).toBe(8);
    });

    it('počítá druhou mocninu při zadání jen jednoho parametru (TODO 5.2)', () => {
      const powerFunc = getPowerOperation();
      expect(powerFunc(2)).toBe(4);
    });
  });

  it('factory pro získání operace (TODO 5.3)', () => {
    expect(typeof operationFactory === 'function').toBeTruthy();
    expect(operationFactory.length).toBeGreaterThan(0);
  });

  it('factory, která obsahuje operaci power (TODO 5.3)', () => {
    const powerFromFactory = operationFactory('power');

    expect(typeof powerFromFactory == 'function').toBeTruthy();
    expect(powerFromFactory(2)).toBe(4);
  });

  it('operaci sčítání dvou čísel (TODO 5.4)', () => {
    const sum = operationFactory('sum');

    expect(sum(1, 2)).toBe(3);
  });

  it('operaci sčítání n čísel (TODO 5.5)', () => {
    const sum = operationFactory('sum');

    expect(sum(1, 2, 3, 4, 5)).toBe(15);
  });

  it('metodu pro aplikaci operace (TODO 5.6)', () => {
    const sum = calculate('sum', 1, 2, 3, 4, 5);
    expect(sum).toBe(15);
  });

  /**
   * Helper funkce, zajišťuje běh testů s factory i bez
   */
  function getPowerOperation() {
    if (typeof power !== 'undefined') {
      return power;
    }

    return operationFactory('power');

  }
});

