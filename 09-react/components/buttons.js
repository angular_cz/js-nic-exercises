import React from 'React';

import Button from './button'
import Operation from './operation'

export default class Buttons extends React.Component {
  constructor() {
    super();
    this.state = {
      currentNumber: null
    }
  }

  concatToString_(value) {
    if (!this.state.currentNumber) {
      return value;
    }

    return String(this.state.currentNumber).concat(value);
  }

  onNumberClick(value) {
    let concatenatedValue = this.concatToString_(value);
    this.setState({
      currentNumber: concatenatedValue
    });

    this.props.onOperandChange(concatenatedValue);
  }

  onOperationClick(event) {
    this.setState({
      currentNumber: null
    });

    this.props.onOperation(event.target.name);
  }

  render() {
    return <div id="buttons">
      <div>
        <Button value={1} onClick={this.onNumberClick.bind(this)}/>
        <Button value={4} onClick={this.onNumberClick.bind(this)}/>
        <Button value={7} onClick={this.onNumberClick.bind(this)}/>
        <Operation name="clear" symbol="C" onClick={this.onOperationClick.bind(this)}/>
      </div>

      <div>
        <Button value={2} onClick={this.onNumberClick.bind(this)}/>
        <Button value={5} onClick={this.onNumberClick.bind(this)}/>
        <Button value={8} onClick={this.onNumberClick.bind(this)}/>
        <Button value={0} onClick={this.onNumberClick.bind(this)}/>
      </div>

      <div>
        <Button value={3} onClick={this.onNumberClick.bind(this)}/>
        <Button value={6} onClick={this.onNumberClick.bind(this)}/>
        <Button value={9} onClick={this.onNumberClick.bind(this)}/>
        <Operation name="equals" symbol="=" onClick={this.onOperationClick.bind(this)}/>
      </div>

      <div>
        <Operation name="plus" symbol="+" onClick={this.onOperationClick.bind(this)}/>
        <Operation name="minus" symbol="-" onClick={this.onOperationClick.bind(this)}/>
        <Operation name="mul" symbol="x" onClick={this.onOperationClick.bind(this)}/>
        <Operation name="div" symbol="/" onClick={this.onOperationClick.bind(this)}/>
      </div>
    </div>
  }
}

Buttons.propTypes = {
  onOperandChange: React.PropTypes.func.isRequired,
  onOperation: React.PropTypes.func.isRequired
};
