import React from 'React'
import Display from './display'

describe('Display', () => {

  const render = (element) => {
    const renderer = React.addons.TestUtils.createRenderer();
    renderer.render(element);
    return renderer.getRenderOutput();
  };

  it('je definovaný', () => {
    expect(Display).toBeDefined();
  });

  it('obsahuje input typu number (TODO 1.1.1)', () => {
    let result = render(<Display value={1}/>);

    expect(result.type).toBe('div');

    const children = result.props.children;
    expect(children.type).toEqual('input');
  });

  it('má definované propTypes pro value (TODO 1.2)', () => {
    expect(Display.propTypes).toBeDefined();
    expect(Display.propTypes.value).toBeDefined();
  });

  describe('input', () => {
    let input;
    beforeEach(() => {
      let result = render(<Display value={111}/>);
      input = result.props.children;
    });

    it('je pouze ke čtení (TODO 1.1.2)', () => {
      expect(input.props.readOnly).toBeDefined();
    });

    it('obahuje hodnotu předanou jako props.value (TODO 1.1.3)', () => {
      expect(input.props.value).toBe(111);
    });

  });
});
