
export class Operation {
  constructor(name) {
    this.name = name;
  }

  calculate(a, b) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };
}

export class Add extends Operation {

  constructor() {
    super('add');
  }

  calculateInternal_(a, b) {
    return a + b;
  };
}

export class Sub extends Operation {

  constructor() {
    super('sub');
  }

  calculateInternal_(a, b) {
    return a - b;
  };
}

export default class Calculator {
  constructor() {
    this.operations = new Map();
  }

  addOperation(operation) {
    this.operations.set(operation.name, operation);
  };

  calculate(name, ...operands) {
    return this.operations.get(name).calculate(...operands);
  }
}

export let calculator = new Calculator();
calculator.addOperation(new Add());
calculator.addOperation(new Sub());

