import {Generator, sequenceGenerator} from './generator-oop'

describe('Generator', () => {
  it('může být instancován (TODO 1.1)', () => {
    expect(typeof new Generator()).toBe('object');
    expect(new Generator() instanceof Generator).toBeTruthy();
  });

  it('má iniciální hodnotu value 0 (TODO 1.2)', () => {
    const generator = new Generator();
    expect(generator.value).toBe(0);
  });

  it('má metodu increase (TODO 1.3)', () => {
    const generator = new Generator();
    expect(typeof generator.increase).toBe("function");
  });

  it('při prvním volání increase vrací iniciální hodnotu (TODO 1.3)', () => {
    const generator = new Generator();
    expect(generator.increase()).toBe(0);
  });

  it('při dalším volání increase zvyšuje hodnotu o 1  (TODO 1.3)', () => {
    const generator = new Generator();

    const first = generator.increase();
    const second = generator.increase();
    expect(second - first).toBe(1);
  });

  it('přijímá iniciální hodnotu jako parametr konstruktoru (TODO 1.4)', () => {
    const generator = new Generator(262);
    expect(generator.increase()).toBe(262);
  });

  it('iniciální hodnota je 0 (TODO 1.4)', () => {
    const generator = new Generator();
    expect(generator.increase()).toBe(0);
  });

  it('byl instancován do proměnné sequenceGenerator (TODO 1.5)', () => {
    expect(sequenceGenerator instanceof Generator).toBeTruthy();
  });

  it('první generovaná hodnota je 1 (TODO 1.6)', () => {
    expect(sequenceGenerator.increase()).toBe(1);
  });
  it('další volání generátoru vrátí 2 (TODO 1.6)', () => {
    expect(sequenceGenerator.increase()).toBe(2);
  });
});

