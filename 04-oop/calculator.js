
// TODO 3.4 - Použijte dědičnost
export class Operation {
  constructor(name) {
    this.name = name;
  }

}

export class Add {
  constructor() {
    // TODO 3.4.1 - Zavolejte konstruktor rodiče
    this.name = "add";
  }

  calculate(a, b) {
    return a + b;
  }

  // TODO 3.1 - Ošetření vstupů operace Add

}

// TODO 3.2 - Vytvořte operaci odčítání

export default class Calculator {
  constructor() {
    this.operations = new Map();
  }

  addOperation(operation) {
    this.operations.set(operation.name, operation);
  };

  calculate(name, ...operands) {
    return this.operations.get(name).calculate(...operands);
  }
}

export let calculator = new Calculator();
calculator.addOperation(new Add());
// TODO 3.3 - Přidejte akci odčítání

